import react from 'react';
import './App.css';
import Header from './components/Header';
import Section from './components/Section';
import SectionBottom from './components/SectionBottom';
import BuySection from './components/BuySection';
import SectionShop from './components/SectionShop';
import SectionItems from './components/SectionItems';
import SectionShopClothes from './components/SectionShopClothes';
import OurClients from './components/OurClients';
import OurBlog from './components/OurBlog';
import Recent from './components/Recent';
import SectionEmail from './components/SectionEmail';
import Footer from './components/Footer';
import PersonList from './components/PersonList';
import CreateCart from './components/CreateCart';
import Ccomponent from './components/Ccomponents';

function App() {
  return (
    <div className="App">
      <CreateCart />
      <Section />
      <SectionBottom />
      <BuySection />
      <SectionShop />
      <SectionItems />
      <SectionShopClothes />
      <OurClients />
      <OurBlog />
      <Recent />
      <PersonList />
      <Ccomponent />
    </div>
  );

}

export default App;
