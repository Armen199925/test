import React from "react";
import "./Contact.css";
import { FaChevronUp } from "react-icons/fa";
import OpenAnswer from "../Contact/OpenAnswer";

function Contact() {
    return (
        <div>
            <div className="contact-us">Contact Us</div>
            <div className="contact-container">
                <div className="ul-menu">
                    <ul>
                        <li>Home &#10097;</li>
                        <li>Contact Us</li>
                    </ul>
                    <hr className="blog-hr" />
                </div>
                <div className="contact-information">
                    <h1 className="contact-bio">Contact Information</h1>
                    <p className="contact-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</p>
                </div>
                <div className="contact-info">
                    <div className="email-address">
                        <div className="email1-img"></div>
                        <h1 className="mail-text">E-mail Address</h1>
                        <p className="phone-txt">mail@example.com</p>
                    </div>
                    <div className="phone-number">
                        <div className="phone-img"></div>
                        <h1 className="mail-text">Phone Number</h1>
                        <p className="phone-txt">(123) 456-7890 / (123) 456-9870</p>
                    </div>
                    <div className="address">
                        <div className="address-img"></div>
                        <h1 className="mail-text">Address</h1>
                        <p className="phone-txt">Lawrence,NY 11345 USA</p>
                    </div>
                    <div className="fax">
                        <div className="fax-img"></div>
                        <h1 className="mail-text">Fax</h1>
                        <p className="phone-txt">1-800-570-7777</p>
                    </div>
                </div>
                <hr />
                <div className="questions">
                    <div className="query">
                        <h1 className="people-ask">People usually ask these</h1>
                        <div className="ask">
                            <div className="question">
                                <FaChevronUp onClick={OpenAnswer} data-target = "1234" className="chevron-up" />
                                <div className="people-question">
                                    <h1 className="ask-one">How can i cancel my order</h1>
                                </div>
                                <div className="answer">
                                    <p   data-target = "12344"  className="answer-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod temp ori ncid
                                        idunt ut labore et dolore magna aliqua.
                                        Venenatis tellus in metus vulp utate eu sceler isque felis. Vel pretium.
                                    </p>
                                </div>
                            </div>
                            <div className="question">
                                <FaChevronUp onClick={OpenAnswer} data-index = "1123" className="chevron-up" />
                                <div className="people-question">
                                    <h1 className="ask-one">How can i cancel my order</h1>
                                </div>
                                <div className="answer">
                                    <p    data-target = "123"  className="answer-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                        sed do eiusmod temp orincid
                                        idunt ut labore et dolore magna aliqua.
                                        Venenatis tellus in metus vulp utate eu sceler isque felis. Vel pretium.
                                    </p>
                                </div>
                            </div>
    
                            
                        </div>
                    </div>
                    <form className="form-contact">
                        <div className="form-group">
                            <label className="label" for="username">Your Name</label>
                            <input type="text" id="username" name="username" className="form-control" />
                            <label for="email_1" className="label">Your Email</label>
                            <input type="email" id="email_1" name="email_1" className="form-control" />
                            <label for="message" className="label">Your Message</label>
                            <textarea cols="30" id="message" rows="8"></textarea>
                            <button className="send-email">SEND NOW</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
};

export default Contact;