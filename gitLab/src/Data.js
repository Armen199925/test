let shop = {
    name: "",
    category: "",
};
let vendor = {
    name: "",
    category: "",
};
let blog = {
    name: "",
    category: "",
};
let pages = {
    name: "",
    category: "",
};
let elements = {
    name: "",
    category: "",
};

let category = {
    name: "",
    category: "or male female"
};

let slideObj = {
    bestSeller: {
        counts: "",
        data: "",
        price: "",
        img: ""
    }
};

let obj =  {
    products: {
        tShirt: {
            counts: "3",
            material: "cotton",
            breeder: "addias",
            price: "1000$",
            color: "red,blue,green",
            img: ""
        },
        shoes:{
            counts: "3",
            material: "leather",
            breeder: "puma",
            price: "100$",
            color: "red,blue,green",
            img: ""
        },
        jeans:{
            counts: "3",
            material: "cotton",
            breeder: "nike",
            price: "1090$",
            color: "red,blue,green",
            img: ""
        },
        tv:{
            counts: "3",
            display: "8.9",
            breeder: "samsung",
            screen: "OLED",
            price: "1000$",
            color: "red,blue,green",
            img: ""
        }
    }
};

let discount = {
    start: "",
    end: "",
    productName: "",
    price: "",
    count: "",
    rating: "",
    img: ""
};

let bestSeller = {
    productName: "",
    price: "",
    rating: "",
    img: ""
};

let topMonth = {
    productName: "",
    categoryName: "",
    img: ""
};

let popularProducts ={
    productName: "",
    rating: "",
    price: "",
    img: ""
};

let offer = {
    productName: "",
    img: ""
};

let clothing = { 
    productName: "",
    img: "",
    price: "",
    rating: ""
};

let electric = { 
    productName: "",
    img: "",
    price: "",
    rating: ""
};

let fashion = {
    productName: "",
    price: "",
    img: ""
};

let homeGarden = {
    productName: "",
    img: "",
    price: "",
    rating: ""
};

let OurClients = {
    imgLogo: "",
};

let ourBlog = {
    img: "",
    text: ""
};

let recent = {
    productName: "",
    img: "",
    hoverText: ""
};

let footerTopMenu = {
    name: "",
    numberPhone: "",
    socialIcons: "",
};

let footerBottom = {
    name: "",
    visaArcaCards: ""
};