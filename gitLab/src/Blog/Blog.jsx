import React from "react";
import './Blog.css';
import { FaChevronRight, FaSearch, FaChevronLeft } from "react-icons/fa";
import NextPopular from "./NextPopular";
import PopularClick from "./PopularClick";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css"
import { useState } from "react";

function Blog() {
  const [calendarValue, onChangeCalendar] = useState(new Date());
  return (
    <div>
      <div className="classic">Classic</div>
      <div className="container">
        <div className="ul-menu">
          <ul>
            <li>Home &#10097;</li>
            <li>Blog &#10097;</li>
            <li>Classic</li>
          </ul>
          <hr className="blog-hr" />
        </div>
      </div>
      <div className="container">
        <section className="section-blog">
          <div className="blog-post">
            <div className="blog_background-image"></div>
            <p className="category-blog">Fashion</p>
            <h1 className="blog-description">New found the men dress for summer</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
            <div className="blog_background-image2"></div>
            <p className="category-blog">Others, Technology</p>
            <h1 className="blog-description">Recognitory the needs is primary condition for design</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
            <div className="blog_background-image3"></div>
            <p className="category-blog">Clothes</p>
            <h1 className="blog-description">New found the womens shirt for summer season</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
            <div className="blog_background-image4"></div>
            <p className="category-blog">Lifestyle</p>
            <h1 className="blog-description">We want to be different and fashion gives to me that outlet</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
            <div className="blog_background-image5"></div>
            <p className="category-blog">Entertainment, Lifestyle, Others</p>
            <h1 className="blog-description">Comes a cool blog post with Images</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
            <div className="blog_background-image6"></div>
            <p className="category-blog">Fashion, Technology</p>
            <h1 className="blog-description">Fusce lacinia arcuet nulla</h1>
            <p className="blog-text">
              Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, e
              get blandit nunc tortor eu nibh.
              Suspendisse potenti.Sed egstas, ant at vulputate volutpat,
              uctus metus libero eu augue, vitae luctus…
            </p>
            <p className="blog-author">by <strong>John Doe</strong> - 03.05.2021</p>
          </div>
          <aside className="aside-blog">
            <div className="blog-container">
              <div className="blog-search">
                <input type="text" className="blog-input" placeholder="Search in Blog" />
                <FaSearch className="blog-search-ico" />
              </div>
              <div className="blog-categories">
                <ul className="ul-categories">
                  <h1>Categories</h1>
                  <li>Entertainment</li>
                  <li>Fashion</li>
                  <li>LifeStyle</li>
                  <li>Others</li>
                  <li>Shoes</li>
                  <li>Technology</li>
                </ul>
              </div>
              <div className="popular-posts">
                <div className="posts-bio">
                  <h1 style={{ fontSize: "18px", color: "black" }} >Popular Posts</h1>
                  <FaChevronLeft onClick={PopularClick} className="btn-left" />
                  <FaChevronRight onClick={NextPopular} className="btn-right" />
                </div>
                <div className="popular">
                  <div className="posts">
                    <div className="posts-image"></div>
                    <div className="post">
                      <p className="posts-data">March 1,2021</p>
                      <br />
                      <p className="posts-text">Fashion Tells about who you are from external point</p>
                    </div>
                  </div>
                  <div className="posts">
                    <div className="posts-image2"></div>
                    <div className="post">
                      <p className="posts-data">March 5,2021</p>
                      <br />
                      <p className="posts-text">New found the men dress for summer</p>
                    </div>
                  </div>
                  <div className="posts">
                    <div className="posts-image3"></div>
                    <div className="post">
                      <p className="posts-data">March 1,2021</p>
                      <br />
                      <p className="posts-text">Cras ornare tristique elit</p>
                    </div>
                  </div>
                </div>
                <div className="next-popular">
                <div className="posts">
                  <div className="next-posts-image"></div>
                  <div className="post">
                    <p className="posts-data">March 1,2021</p>
                    <br />
                    <p className="posts-text">Vivamus vesitbulum ntulla nec ante</p>
                  </div>
                </div>
                <div className="posts">
                  <div className="next-posts-image2"></div>
                  <div className="post">
                    <p className="posts-data">March 5,2021</p>
                    <br />
                    <p className="posts-text">Fashion you are from external point</p>
                  </div>
                </div>
                <div className="posts">
                  <div className="next-posts-image3"></div>
                  <div className="post">
                    <p className="posts-data">March 1,2021</p>
                    <br />
                    <p className="posts-text">Comes a cool blog post with images</p>
                  </div>
                </div>
              </div>
              </div>
              <div className="custom-block">
                <h1 className="custom-block-name">Custom Block</h1>
                <p className="custom-block-description">Fringilla urna porttitor rhoncus dolor purus.
                  Luctus veneneratis lectus magna fring. Suspendisse potenti.
                  Sed egestas, ante et vulputate volutpat, uctus metus libero.
                </p>
              </div>
              <h1 className="browse" >Browse Tags</h1>
              <div className="tags">
                <div className="blog-tags1">Fashion</div>
                <div className="blog-tags2">Style</div>
                <div className="blog-tags3">Travel</div>
                <div className="blog-tags4">Women</div>
                <div className="blog-tags5">Men</div>
                <div className="blog-tags6">Hobbies</div>
                <div className="blog-tags7">Shopping</div>
                <div className="blog-tags8">Photography</div>
              </div>
            </div>
            <Calendar value={calendarValue} onChange={onChangeCalendar} />
          </aside>
        </section>
      </div>
    </div>
  )
};

export default Blog;