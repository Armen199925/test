import React from "react";


function NextPopular() {
    let popularDiv = document.querySelector(".popular");
    popularDiv.style.display = "none";
    let nextPopularDiv = document.querySelector(".next-popular");
    nextPopularDiv.style.display = "flex";
};

export default NextPopular;