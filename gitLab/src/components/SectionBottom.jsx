import React from "react";
import "../css/SectionBottom.css";

function SectionBottom() {
    return (
        <div>
            <div className="container-2">
                <div className="panel">
                    <div className="shipping-div">
                        <img className="truck-img" src="../img/truck.png" />
                        <div className="shipping-txt">
                            <h1 className="free-shipping">Free Shipping & Returns</h1>
                            <p className="for-orders">For all orders over $99</p>
                        </div>
                    </div>
                    <div className="shipping-div">
                        <img className="truck-img" src="../img/school-bag.png" />
                        <div className="shipping-txt">
                            <h1 className="free-shipping">Free Shipping & Returns</h1>
                            <p className="for-orders">For all orders over $99</p>
                        </div>
                    </div>
                    <div className="shipping-div">
                        <img className="truck-img" src="../img/mail.png" />
                        <div className="shipping-txt">
                            <h1 className="free-shipping">Free Shipping & Returns</h1>
                            <p className="for-orders">For all orders over $99</p>
                        </div>
                    </div>
                    <div className="shipping-div">
                        <img className="truck-img" src="../img/comment.png" />
                        <div className="shipping-txt">
                            <h1 className="free-shipping">Free Shipping & Returns</h1>
                            <p className="for-orders">For all orders over $99</p>
                        </div>
                    </div>
                </div>
                <section className="section_bottom">
                    <div className="discounts">
                        <img className="discounts-img" src="../img/discounts.jpg" />
                    </div>
                    <div className="accessories">
                        <img className="accessories-img" src="../img/accessories.png" />
                    </div>
                </section>
            </div>
        </div>
    )
}

export default SectionBottom;