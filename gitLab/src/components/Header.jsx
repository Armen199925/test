import React from "react";
import "../css/header.css";
import { FaShoppingCart } from "react-icons/fa";
import { useState } from "react";
import OpenCart from "./OpenCart";
import CloseCart from "./CloseCart";
import {
    useParams,
    Link
  } from "react-router-dom";
  import { Routes  } from 'react-router-dom';
  import { Route  } from 'react-router-dom';
  import { BrowserRouter } from "react-router-dom";
  import Blog from "../Blog/Blog";

function Header() {
    let [cartOpen, setCartOpen] = useState(false);
    return (
        <div>
            <header className="header">
                <div className="header-top">
                    <div className="container">
                        <div className="header-left">
                            <p className="welcome-msg">
                                WELCOME TO WOLMART STORE MESSAGE OR REMOVE IT!
                            </p>
                        </div>
                        <div className="header-right">
                            <div className="dropdown">
                                <div className="sl-nav">
                                    <ul>
                                        <li className="valute">USD<b></b>
                                            <div className="triangle"></div>
                                            <ul className="dropdown-ul">
                                                <li className="usd"><div id="USD"></div> <span className="active">USD</span></li>
                                                <li className="eur"><div id="EUR"></div><span>EUR</span></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <a>
                                    <img className="chevron" src="../img/chevron.png" />
                                </a>
                            </div>
                            <div className="dropdown">
                                <div className="dropdown-box">
                                    <div className="country">
                                        <ul className="country-ul">
                                            <li className="country-li">ENG
                                                <img className="chevron" src="../img/chevron.png" />
                                                <div className="country-select"></div>
                                                <ul className="country-dropdown">
                                                    <img className="eng-logo" src="../img/flag.png" /> <li className="england"><div id="england">
                                                    </div><span className="active">ENG</span></li>
                                                    <img className="fra-logo" src="../img/fra.png" /><li className="france"><div id="france">
                                                    </div><span className="active">FRA</span></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <span className="divider d-lg-show"></span>
                            <Link to="/Blog">Blog</Link>
                            <Link to="/Contact">Contact Us</Link>
                            <Link to="/">Home</Link>               
                            <div className="sign-in">
                                <a href="#" className="d-lg-show">
                                    <img src="../img/user.png" className="user-img" />
                                    "Sign In"
                                </a>
                                <span className="delimiter d-lg-show">/</span>
                                <a href="#" className="ml-0 d-lg-show login register">Register</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-middle">
                    <div className="logo-left">
                        <div className="logo">
                            <img className="logo-img" alt="logo" src="../img/logo.png" />
                        </div>
                        <div className="search">
                            <div className="category">
                                <select>
                                    <option>All Categories</option>
                                    <option>OK</option>
                                    <option>OK</option>
                                    <option>OK</option>
                                </select>
                            </div>
                            <input type="text" placeholder="Search in..." className="search-panel"
                            />
                            <img className="search-ico" src="public/../img/search.png" />
                        </div>
                    </div>
                    <div className="header-right">
                        <div className="call">
                            <div className="call-icon">
                                <img className="call-img" src="public/../img/call.png" />
                            </div>
                            <p className="call-number">
                                Call Us Now:<br /> 044-83-84-84
                            </p>
                        </div>
                        <div className="icons">
                            <div className="heart-ico">
                                <img className="heart-img" src="../img/heart.png" />
                                <h1 className="heart-name">Wishlist</h1>
                            </div>
                            <div className="balance-ico">
                                <img className="balance-img" src="../img/balance.png" />
                                <h1 className="balance-name">Compare</h1>
                            </div>
                            <div className="cart-ico" >
                                <FaShoppingCart  onClick={OpenCart}  className="fa-shopping-cart" />
                                <h1 className="cart-name">Cart</h1>
                                <div className="cart-open">
                                    <button className="deleteCart" onClick={CloseCart}>X</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="header-bottom sticky content fix-top sticky-header has-dropdown">
                    <div className="container">
                        <div className="inner-wrap">
                            <div className="header-bottom-left">
                                <div className="category-dropdown">
                                    <div className="burger-menu"></div>
                                    <h1 className="category-text">
                                        Browse Categories
                                    </h1>
                                    <img className="down-chevron" src="../img/down-chevron.png" />
                                </div>
                                <nav className="navigation">
                                    <ul className="menu">
                                        <li>Home</li>
                                        <li>Shop<img className="down-chevron" src="../img/down-chevron.png" /></li>
                                        <li>Vendor <img className="down-chevron" src="../img/down-chevron.png" /></li>
                                        <li>Blog <img className="down-chevron" src="../img/down-chevron.png" /></li>
                                        <li>Pages <img className="down-chevron" src="../img/down-chevron.png" /></li>
                                        <li>Elements <img className="down-chevron" src="../img/down-chevron.png" /></li>
                                    </ul>
                                </nav>
                            </div>
                            <div className="header-bottom-right">
                                <div className="track">
                                    <img className="track-img" src="../img/track.png" />
                                    <h1 className="track-txt">Track Order</h1>
                                </div>
                                <div className="deals">
                                    <img className="track-img" src="../img/deals.png" />
                                    <h1 className="track-txt">Daily Deals</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    );
};

export default Header;