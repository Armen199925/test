import React from "react";
import "../css/SectionEmail.css";


function SectionEmail() {
    return (
        <div>
            <section className="section-email">
                <div className="container">
                    <div className="subscribe">
                        <div className="newsletter">
                            <img className="email-img" src="../img/email.png" />
                            <div className="email-txt">
                                <h1 className="email-text">SUBSCRIBE TO OUR NEWSLETTER</h1>
                                <p className="email-text">Get all the latest information on Events,Sales and Offers</p>
                            </div>
                        </div>
                        <div className="email">
                            <input className="email-input" placeholder="Your E-mail Address" type="email" />
                            <button className="email-btn">SUBSCRIBE &#8594;</button>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    )
};

export default SectionEmail;