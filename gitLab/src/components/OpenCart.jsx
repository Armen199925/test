import React from "react";

function OpenCart () {
    let cart = document.querySelector(".cart-open");

    console.log(cart.style.display)


    if(cart.style.display == "none" || cart.style.display == ""){
    
    cart.style.position = "relative";
    cart.style.display = "block"
    cart.style.top = "0";
    cart.style.right = "10px";
    cart.style.background = "green";
    cart.style.display = "flex";
    cart.style.justifyContent = "space-evenly";
    cart.style.alignItems = "flex-start";
    cart.style.flexDirection = "column";
    cart.style.padding = "20px"
    let deleteCart = document.querySelector("button");
    deleteCart.className = ".delete-cart";
    deleteCart.innerHTML = "X"
    deleteCart.style.position = "absolute";
    deleteCart.style.top = "0";
    deleteCart.style.right = "37px";
    deleteCart.style.width = "10px";
    deleteCart.style.height = "20px";
    deleteCart.style.outline = "none";
    deleteCart.style.border = "none";
    deleteCart.style.fontSize = "15px"
    cart.append(deleteCart)
    let cartDiv = document.querySelector(".cart-div");
    let productDiv = document.createElement("div");
    productDiv.className = "new-product-div"
    productDiv.style.width = "400px";
    productDiv.style.height = "100px";
    productDiv.style.display = "flex";
    productDiv.style.justifyContent = "space-around";
    productDiv.style.alignItems = "center";
    let b = JSON.parse(localStorage.getItem("new"));
    console.log(b)
    if(b !== null){
        let div = document.createElement("div");
        div.className = "class";
        div.style.width = "303px";
        div.style.height = "100px";
        div.style.display = "flex";
        div.style.alignItems = "flex-start";
        div.style.flexDirection = "column";
        div.style.marginTop = "20px";
        cart.append(div);
        let productDiv = document.createElement("div");
        productDiv.className = "product-div"
        productDiv.style.width = "400px";
        productDiv.style.height = "100px";
        productDiv.style.display = "flex";
        productDiv.style.justifyContent = "space-around";
        productDiv.style.alignItems = "center";
        let img = document.createElement("img");
        img.className = "new-img";
        img.style.width = "150px";
        img.style.height = "100px";
        img.src = b.img
        productDiv.append(img)
        let productName = document.createElement("h1");
        productName.style.margin = "0";
        productName.style.padding = "0";
        productName.style.fontSize = "13px";
        productName.style.width = "60px";
        productName.style.height = "60px";
        productName.style.color = "black";
        productName.innerHTML = b.name
        productDiv.append(productName);
        let priceDiv = document.createElement("h1");
        priceDiv.className = "price-div";
        priceDiv.style.fontSize = "15px";
        priceDiv.style.color = "black";
        priceDiv.style.height = "60px";
        priceDiv.innerHTML = `$` + b.price
        productDiv.append(priceDiv)
        let eleem = document.createElement("h1");
        eleem.innerHTML = b.count
        eleem.style.fontSize = "15px";
        eleem.style.width = "60px";
        eleem.style.height = "60px";
        eleem.style.color = "black";
        productDiv.append(eleem)
        let deleteBtn = document.createElement("button");
        deleteBtn.className = "delete-btn";
        deleteBtn.innerHTML = "X";
        deleteBtn.style.position = "relative";
        deleteBtn.style.width = "10px";
        deleteBtn.style.fontSize = "12px";
        deleteBtn.style.height = "12px";
        deleteBtn.style.border = "none";
        deleteBtn.style.outline = "none";
        deleteBtn.onclick = function () {
           div.style.display = "none"
           localStorage.removeItem("new")
        }
        div.append(deleteBtn)
        div.append(productDiv)
    } 
  
    }else{
     cart.style.display = "none"
     let div = document.querySelector(".cart-open")
     div.innerHTML = ""
    
    }

};

export default OpenCart;