import React from "react";
import "../css/Footer.css";

function Footer() {
    return (
        <div>
            <footer className="footer">
                <div className="container">
                    <footer className="footer-header">
                        <div className="footer-menu">
                            <img src="../img/logo.png" className="footer-logo" />
                            <p className="footer-menu-text">Got Question?Call us 24/7</p>
                            <h3 className="number-call">1-800-570-7777</h3>
                            <p className="footer-menu-text">Register now to get updates on pronot get</p>
                            <p className="footer-menu-text">up icons & coupons ster now toon.</p>
                            <div className="img-social">
                                <img className="img-social-icons" src="../img/facebook.png" />
                                <img className="img-social-icons" src="../img/twitter.png" />
                                <img className="img-social-icons" src="../img/instagram.png" />
                                <img className="img-social-icons" src="../img/youtube.png" />
                                <img className="img-social-icons" src="../img/pinterest.png" />
                            </div>
                        </div>
                        <div className="footer-ul">
                            <ul className="footer-ul">
                                <h1 className="company">Company</h1>
                                <li className="footer-li">About Us</li>
                                <li className="footer-li">Team Member</li>
                                <li className="footer-li">Career</li>
                                <li className="footer-li">Contact Us</li>
                                <li className="footer-li">Affilate</li>
                                <li className="footer-li">Order History</li>
                            </ul>
                        </div>
                        <div className="footer-ul">
                            <ul className="footer-ul">
                                <h1 className="company">My Account</h1>
                                <li className="footer-li">Track My Order</li>
                                <li className="footer-li">View Cart</li>
                                <li className="footer-li">Sign In</li>
                                <li className="footer-li">Help</li>
                                <li className="footer-li">My Wishlist</li>
                                <li className="footer-li">Privacy Policy</li>
                            </ul>
                        </div>
                        <div className="footer-ul">
                            <ul className="footer-ul">
                                <h1 className="company">Customer Service</h1>
                                <li className="footer-li">Payment Methods</li>
                                <li className="footer-li">Money-back guarantee!</li>
                                <li className="footer-li">Product Returns</li>
                                <li className="footer-li">Support Center</li>
                                <li className="footer-li">Shipping</li>
                                <li className="footer-li">Team and Conditions</li>
                            </ul>
                        </div>
                    </footer>
                </div>
                <div className="container">
                    <hr />
                </div>
                <div className="container">
                    <footer className="footer-bottom">
                        <pre className="about-having">Consumer Electric:
                            TV Television |
                            Air Condition |
                            Refrigerator |
                            Washing Machine |
                            Audio Speaker |
                            Security Camera |
                            View All |
                        </pre>
                        <pre className="about-having">
                            Clothing & Apparel: |
                            Men's T-shirt  |
                            Dresses  |
                            Men's Sneacker |
                            Leather Backpack |
                            Watches |
                            Jeans |
                            Sunglasses |
                            Boots |
                            Rayban |
                            Acccessories |
                        </pre>
                        <pre className="about-having">
                            Home, Garden & Kitchen: |
                            Sofa |
                            Chair |
                            Bed Room |
                            Living Room |
                            Cookware |
                            Utensil |
                            Blender |
                            Garden Equipments |
                            Decor |
                            Library |
                        </pre>
                        <pre className="about-having">
                            Health & Beauty: |
                            Skin Care |
                            Body Shower |
                            Makeup |
                            Hair Care |
                            Lipstick |
                            Perfume |
                            View all |
                        </pre>
                        <pre className="about-having">
                            Jewelry & Watches: |
                            Necklace |
                            Pendant |
                            Diamond Ring |
                            Silver Earing |
                            Leather Watcher |
                            Rolex |
                            Gucci |
                            Australian Opal |
                            Ammolite |
                            Sun Pyrite |
                        </pre>
                        <pre className="about-having">
                            Computer & Technologies: |
                            Laptop |
                            iMac |
                            Smartphone |
                            Tablet |
                            Apple |
                            Asus |
                            Drone |
                            Wireless Speaker |
                            Game Controller |
                            View all |
                        </pre>
                        <div className="container">
                            <hr />
                        </div>
                        <div className="copyright">
                            <div>
                                <p className="copyright-text">Copyright © 2021 Wolmart Store. All Rights Reserved.
                                </p>
                            </div>
                            <div>
                                
                                <div className="payments">
                                    <p className="copyright-text">Were Using safe payment for</p>
                                    <div className="maestro"></div>
                                    <div className="discover"></div>
                                    <div className="visa"></div>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </footer>
        </div>
    )
};

export default Footer;