import React from "react";
import { Component } from "react";

export default class Ccomponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        }
        
    }
    componentDidMount() {
        fetch("https://37.186.119.16:9443/list")
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });       
                }
            )

    }
    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <p>Error {error.message}</p>
        }
        else if (!isLoaded) {
            return <p>Loading...</p>
        } else {
            return <ul>
                {items.map(item => (
                    <li key={item.name}>
                        {item.strDrink}
                    </li>
                ))}
            </ul>
        }
    }
}
