import React from "react";
import "../css/section.css";
import {rightClick , BackClick} from "./RightClick";
import {FaChevronLeft} from "react-icons/fa";
import {FaChevronRight} from "react-icons/fa";


function Section() {
    return (
        <div>
            <section data="1" className="section">
                <FaChevronLeft className="chevron-left" onClick={BackClick} />
                <div className="container">
                    <section className="section-left">
                        <ul className="menu-ul">
                            <li className="menu-li"><img src="../img/tshirt.png" className="menu-img"/> Fashion</li>
                            <li className="menu-li"><img src="../img/home.png" className="menu-img"a/> Home & Garden</li>
                            <li className="menu-li"><img src="../img/responsive.png" className="menu-img"/>  Electronics</li>
                            <li className="menu-li"><img src="../img/furniture.png" className="menu-img"/>  Furniture</li>
                            <li className="menu-li"><img src="../img/heart-shop.png" className="menu-img"/>  Healty</li>
                            <li className="menu-li"><img src="../img/gift.png" className="menu-img"/>  Gift</li>
                            <li className="menu-li"><img src="../img/games.png" className="menu-img"/>  Toy & Games</li>
                            <li className="menu-li"><img src="../img/cooking.png" className="menu-img"/>  Cooking</li>
                            <li className="menu-li"><img src="../img/smartphone.png" className="menu-img"/>  Smart Phone</li>
                            <li className="menu-li"><img src="../img/camera-shop.png" className="menu-img"/>  Cameras</li>
                            <li className="menu-li"><img src="../img/diamond.png" className="menu-img"/>  Accessories</li>
                            <a>View All CATEGORIES </a>
                        </ul>
                        
                    </section>
                    <FaChevronRight  className="chevron-right" onClick={rightClick} />
                </div>
            </section>
        </div>
    )
};


export default Section;
