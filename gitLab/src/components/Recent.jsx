import React from "react";
import "../css/Recent.css";

function Recent() {
    return (
        <div>
            <section className="recent">
                <h1 className="recent-header">Your Recent Views</h1>
                <hr />
                <div className="container">
                    <div className="recent-products">
                        <div className="recent-bag">
                            <div className="recent-bag-hover">
                                <h1 className="hover-text">Womens fashion Handbag</h1>
                            </div>
                        </div>

                        <div className="recent-pan">
                            <div className="recent-pan-hover">
                                <h1 className="hover-text">Elyctric Frying Pan</h1>
                            </div>
                        </div>
                        <div className="recent-roll">
                            <div className="recent-roll-hover">
                                <h1 className="hover-text">Back Winter Skating</h1>
                            </div>
                        </div>
                        <div className="recent-tv">
                            <div className="recent-tv-hover">
                                    <h1 className="hover-text">HD television</h1>
                            </div>
                        </div>
                        <div className="recent-sofa">
                            <div className="recent-sofa-hover">
                                <h1 className="hover-text">Home Sofa</h1>
                            </div>
                        </div>
                        <div className="recent-airpods">
                            <div className="recent-airpods-hover">
                                <h1 className="hover-text">USB RECEIPT </h1>
                            </div>
                        </div>
                        <div className="recent-cooker">
                            <div className="recent-cooker-hover">
                                <h1 className="hover-text">electric rice-cooker </h1>
                            </div>
                        </div>
                        <div className="recent-lamp">
                            <div className="recent-lamp-hover">
                                <h1 className="hover-text">Table Lamp</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default Recent;