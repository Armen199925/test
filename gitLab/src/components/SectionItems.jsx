import React from "react";
import "../css/SectionItems.css";
import { FaCartPlus, FaHeart, FaSearch, FaBalanceScale } from "react-icons/fa";
import AddToCart from "./AddToCart";

function SectionItems() {
    return (
        <div>
            <section className="section-items">
                <div className="container-item">
                    <h1>Popular Departments</h1>
                    <div className="departments">
                        <div className="arrrivals">NEW ARRIVALS</div>
                        <div className="arrrivals">BEST SELLER</div>
                        <div className="arrrivals">MOST POPULAR</div>
                        <div className="arrrivals">FEATURED</div>
                    </div>
                    <div className="section-products">
                        <div className="product">
                            <div className="product-img-towel">
                                <FaCartPlus onClick={AddToCart} className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Fashion Blue Towel</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-hairdye">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Womens hairdye</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-transformer">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Data Transformer Tool</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-backpack">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Comfortable Backpack</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-handbag">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Womens White Handbag</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-hat">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Classic Hat</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$53.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-iphone">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Multi Functional Apple Iphone</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-mac">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Apple Super Notecom</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-comforter">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Womens Comforter</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>
                        <div className="product">
                            <div className="product-img-music">
                                <FaCartPlus className="fa-cart-plus" />
                                <FaHeart className="fa-cart-plus" />
                                <FaSearch className="fa-cart-plus" />
                                <FaBalanceScale className="fa-cart-plus" />
                            </div>
                            <div className="product-txt">
                                <h1 className="product-name">Multi-colorful Music</h1>
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star.png" />
                                <img className="stars-images" src="../img/star1.png" />
                                <h1 className="price_product">$26.55-$29.99</h1>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="bottom-products">
                    <div className="container">
                        <div className="products-show">
                            <div className="left-products">
                                <div className="left-products-txt">
                                    <h3 className="left-products-text">
                                    Natural Process 
                                    <br />
                                    Cosmetic Makeup 
                                    <br />
                                    Professional
                                    </h3>
                                </div>
                                <div className="shop-now-left">
                                    <h6 className="shop-now-left-txt">SHOP NOW &#8594;</h6>
                                </div>
                            </div>
                            <div className="right-products">
                            <div className="right-products-txt">
                                    <h3 className="right-products-text">
                                    Natural Process 
                                    <br />
                                    Cosmetic Makeup 
                                    <br />
                                    Professional
                                    </h3>
                                </div>
                                <div className="shop-now-left">
                                    <h6 className="shop-now-right-txt">SHOP NOW &#8594;</h6>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </div>
    )
};

export default SectionItems;