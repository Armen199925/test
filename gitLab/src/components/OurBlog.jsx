import React from "react";
import "../css/OurBlog.css";


function OurBlog() {
    return (
        <div>
            <section className="our-blog">
                <div className="our-blog-header">
                    <h1 className="our-blog-txt">From Our Blog</h1>
                    <hr />
                    <div className="container">
                        <div className="blog">
                            <div className="first-blog">
                                <div className="first-blog-img"></div>
                                <h2 className="blog-by">by John Doe - 03.05.2021 </h2>
                                <h1 className="blog-txt">Aliquam tincidunt mauris eurisus</h1>
                                <a className="read-more">Read More &#8594;</a>
                            </div>
                            <div className="second-blog">
                                <div className="second-blog-img"></div>
                                <h2 className="blog-by">by John Doe - 03.05.2021 </h2>
                                <h1 className="blog-txt">Aliquam tincidunt mauris eurisus</h1>
                                <a className="read-more">Read More &#8594;</a>
                            </div>
                            <div className="third-blog">
                                <div className="third-blog-img"></div>
                                <h2 className="blog-by">by John Doe - 03.05.2021 </h2>
                                <h1 className="blog-txt">Aliquam tincidunt mauris eurisus</h1>
                                <a className="read-more">Read More &#8594;</a>
                            </div>
                            <div className="last-blog">
                                <div className="last-blog-img"></div>
                                <h2 className="blog-by">by John Doe - 03.05.2021 </h2>
                                <h1 className="blog-txt">Aliquam tincidunt mauris eurisus</h1>
                                <a className="read-more">Read More &#8594;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default OurBlog;