import React from "react";
import { FaChevronRight } from "react-icons/fa";
import { FaChevronLeft } from "react-icons/fa";
import { FaShoppingCart } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaPinterestP } from "react-icons/fa";
import { FaWhatsapp } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa";
import { FaHeart } from "react-icons/fa";
import { FaBalanceScale } from "react-icons/fa";
import Next from "../components/Next";
import Previous from "./Previous";
import NextClothes from "./NextClothes";
import Clothes from "./Clothes";
import "../css/BuySection.css";
import Onclick from "./OnClick";
import OnclickSecond from "./OnClickSecond";
import OnclickThird from "./OnClickThird";
import OnclickNextSecond from "./OnClickNextSecond";
import OnclickNextThird from "./OnClickNextThird";
import OnclickNextLast from "./OnClickNextLast";
import AddToCart from "./AddToCart";
import Last from "./Last";
import OcNext from "./OcNext";

function BuySection() {
    let minusBtn = document.querySelector(".minus-btn");
    let plusBtn = document.querySelector(".plus-btn");

    function showAdd() {
        let num = document.querySelector(".num");
        num.value = ++num.value
        if (num.value >= 10) {
            num.value = 10;
        }
    };
    function showRemove() {
        let num = document.querySelector(".num");
        num.value = --num.value;
        if (num.value <= 1) {
            num.value = 1;
        }
    }

    return (
        <div>
            <section className="buysection">
                <div className="container">
                    <div className="clothes-div">
                        <div className="top-div">
                            <div className="text-div">
                                <h1 className="deals-hot">Deals Hot of The Day</h1>
                                <div>
                                    <FaChevronLeft className="btn-left" onClick={Clothes}
                                        style={{ width: "10px", cursor: "pointer", marginLeft: "5px" }}>
                                    </FaChevronLeft>
                                    <FaChevronRight className="btn-right" onClick={NextClothes}
                                        style={{ width: "10px", cursor: "pointer", color: "black", marginLeft: "5px" }}>
                                    </FaChevronRight>
                                </div>
                            </div>
                            <div>
                                <hr />
                            </div>
                        </div>
                        <div className="clothes">
                            <div className="buy-coat">
                                <div className="img-clothes">
                                    <div className="small-div">
                                        <div className="clothes-img-small1" onClick={Onclick}></div>
                                        <div className="clothes-img-small2" onClick={OnclickSecond}></div>
                                        <div className="clothes-img-small3" onClick={OnclickThird}></div>
                                        <div className="clothes-img-small4" onClick={Last}></div>
                                    </div>
                                    <div className="big-div">
                                        <img className="add-img" id="3" src="../img/cloat2.jpg" />
                                    </div>
                                </div>
                                <div className="price">
                                    <h1 className="name-product">Coat Pool Comfort Jacket</h1>
                                    <hr />
                                    <h1 className="dollar">$150</h1>
                                    <div className="stars-icon">
                                        <div className="stars-img">
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star1.png" />
                                        </div>
                                        <h1 style={{ fontSize: "15px" }}>(3 Reviews)</h1>
                                    </div>
                                    <div className="size">
                                        <div className="size-txt">
                                            <h1 className="text-size">Size:</h1>
                                            <div className="select-size">
                                                <button className="size-click">Extra Large</button>
                                                <button className="size-click">Large</button>
                                                <button className="size-click">Medium</button>
                                                <button className="size-click">Small</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr className="hr" />
                                    <div className="add_cart">
                                        <div className="select-number">
                                            <button onClick={showRemove} className="minus-btn">-</button>
                                            <button onClick={showAdd} className="plus-btn">+</button>
                                            <input type="number" className="num" value={1} />
                                        </div>
                                        <div className="add-cart" onClick={AddToCart}>
                                            <FaShoppingCart />
                                            <h4 className="add-to-cart">ADD TO CART</h4>
                                        </div>
                                    </div>
                                    <div className="social-icons">
                                        <div className="facebook-ico"><FaFacebookF /></div>
                                        <div className="facebook-ico"><FaTwitter /></div>
                                        <div className="facebook-ico"><FaPinterestP /></div>
                                        <div className="facebook-ico"><FaWhatsapp /></div>
                                        <div className="facebook-ico"><FaLinkedinIn /></div>
                                        <div className="heart-ico"><FaHeart /></div>
                                        <div className="heart-ico"><FaBalanceScale /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="clothes-next">
                            <div className="buy-coat">
                                <div className="img-clothes">
                                    <div className="small-div">
                                        <div className="clothes-next-img-small1" onClick={OcNext}></div>
                                        <div className="clothes-next-img-small2" onClick={OnclickNextSecond}></div>
                                        <div className="clothes-next-img-small3" onClick={OnclickNextThird}></div>
                                        <div className="clothes-next-img-small4" onClick={OnclickNextLast}></div>
                                    </div>
                                    <div className="big-div-next">
                                        <img className="addD-img" id="2" src="../img/1.jpg" />
                                    </div>
                                </div>
                                <div className="price">
                                    <h1 className="name-product">Coat Pool Comfort Jacket</h1>
                                    <hr />
                                    <h1 className="dollar">$150.00-$180.00</h1>
                                    <div className="stars-icon">
                                        <div className="stars-img">
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star.png" />
                                            <img className="stars-images" src="../img/star1.png" />
                                        </div>
                                        <h1 style={{ fontSize: "15px" }}>(3 Reviews)</h1>
                                    </div>
                                    <div className="size">
                                        <div className="size-txt">
                                            <h1 className="text-size">Size:</h1>
                                            <div className="select-size">
                                                <button className="size-click">Extra Large</button>
                                                <button className="size-click">Large</button>
                                                <button className="size-click">Medium</button>
                                                <button className="size-click">Small</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr style={{ marginTop: "10px" }} />
                                    <div className="add_cart">
                                        <div className="select-number">
                                            <button onClick={showRemove} className="minus-btn">-</button>
                                            <button onClick={showAdd} className="plus-btn">+</button>
                                            <input type="number" className="num" value={1} />
                                        </div>
                                        <div className="add-cart">
                                            <FaShoppingCart />
                                            <h4 style={{ fontSize: "15px" }}>ADD TO CART</h4>
                                        </div>
                                    </div>
                                    <div className="social-icons">
                                        <div className="facebook-ico"><FaFacebookF /></div>
                                        <div className="facebook-ico"><FaTwitter /></div>
                                        <div className="facebook-ico"><FaPinterestP /></div>
                                        <div className="facebook-ico"><FaWhatsapp /></div>
                                        <div className="facebook-ico"><FaLinkedinIn /></div>
                                        <div className="heart-ico"><FaHeart /></div>
                                        <div className="heart-ico"><FaBalanceScale /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="top-best">
                        <div className="top-seller">
                            <h1 className="seller-txt">Top 20 Best Seller</h1>
                            <div>
                                <FaChevronLeft className="left-previous" onClick={Previous}
                                    style={{ width: "10px", cursor: "pointer", color: "grey" }}>
                                </FaChevronLeft>
                                <FaChevronRight onClick={Next} className="right-next"
                                    style={{ width: "10px", cursor: "pointer", color: "black", marginLeft: "5px" }}>
                                </FaChevronRight>
                            </div>
                        </div>
                        <hr />
                        <div className="products">
                            <div className="cooker"></div>
                            <div className="cooker-price">
                                <h1 className="kitchen">Kitchen Cooker</h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                </div>
                                <h1 className="dollar">$150.60</h1>
                            </div>
                            <div className="camera"></div>
                            <div className="camera-price">
                                <h1 className="camera-text">Professional Pixel Camera</h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                </div>
                                <div className="discounts">
                                    <h1 className="dollar">$215.68</h1>
                                    <h1 className="dollar-underline">$230.45</h1>
                                </div>
                            </div>
                            <div className="wear"></div>
                            <div className="wear-price">
                                <h1 className="camera-text">Sport Womens Wear</h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                </div>
                                <div className="discounts">
                                    <h1 className="dollar">$220.00</h1>
                                    <h1 className="dollar-underline">$300.62</h1>
                                </div>
                            </div>
                        </div>
                        <div className="products-next">
                            <div className="weather"></div>
                            <div className="cooker-price">
                                <h1 className="kitchen">Latest Speaker</h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                </div>
                                <h1 className="dollar">$150.60</h1>
                            </div>
                            <div className="watch"></div>
                            <div className="camera-price">
                                <h1 className="camera-text">Mens Block Wrist Watch </h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star1.png" />
                                </div>
                                <div className="discounts">
                                    <h1 className="dollar">$135.60</h1>
                                    <h1 className="dollar-underline">$155.70</h1>
                                </div>
                            </div>
                            <div className="washing"></div>
                            <div className="wear-price">
                                <h1 className="camera-text">Wash Machine</h1>
                                <div className="stars-img">
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                    <img className="cooker-images" src="../img/star.png" />
                                </div>
                                <div className="discounts">
                                    <h1 className="dollar">$220.00</h1>
                                    <h1 className="dollar-underline">$300.62</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div></div>
                </div>
            </section>
        </div>
    )
};

export default BuySection;