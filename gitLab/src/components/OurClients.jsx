import React from "react";
import "../css/OurClients.css";

function OurClients () {
    return(
        <div>
            <section className="clients-header">
                <h1 className="clients-text">Our Clients</h1>
                <div className="container">
                    <div className="clients">
                        <div className="sterling"></div>
                        <div className="ns"></div>
                        <div className="galaxy"></div>
                        <div className="skysuite"></div>
                        <div className="red"></div>
                        <div className="rightchek"></div>
                        <div className="greengrass"></div>
                        <div className="kinova"></div>
                        <div className="sass"></div>
                        <div className="elegant"></div>
                        <div className="node"></div>
                        <div className="skillstar"></div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default OurClients ;