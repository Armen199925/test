import React from "react";

function Next() {
        let products = document.querySelector(".products");
        let nextBtn = document.querySelector(".right-next");
        products.style.display = "none";
        let productsNext = document.querySelector(".products-next");
        productsNext.style.display = "flex";
};

export default Next;