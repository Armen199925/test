import React from "react";
import "../css/SectionShopClothes.css";
import { FaCartPlus, FaHeart, FaSearch, FaBalanceScale } from "react-icons/fa";

function SectionShopClothes() {
    return (
        <div>
            <section className="shop-clothes">
                <div className="container">
                    <div className="store-header">
                        <h1>Clothing & Apparel</h1>
                        <div className="store">
                            <div className="store-left"></div>
                            <div className="store-right">
                                <div className="product">
                                    <div className="product-img-clothing">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Mens Clothing</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-bag">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Womens Fashion Handbag</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-rolls">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Black Winter Skating</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-jacket">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Sport Womens Wear</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-travel">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Best Travel Bag</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-shoes">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Gray Leather Shoes</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-watch">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Mens Black Wrist Watch</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-hat">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Womens Hiking Hat</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="store-header">
                        <h1>Consumer Electric</h1>
                        <div className="store">
                            <div className="store-left-camer"></div>
                            <div className="store-right">
                                <div className="product">
                                    <div className="product-img-camera">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Professional Pixel Camera</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-wash">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Wash Machine</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-tv">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">HD Television</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-mac">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Data transformer tool</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-watch">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Latest Speaker</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-camer">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">USB Receipt</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <img className="stars-images" src="../img/star1.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-airpods">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Mens Black Wrist Watch</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                                <div className="product">
                                    <div className="product-img-powershell">
                                        <FaCartPlus className="fa-cart-plus" />
                                        <FaHeart className="fa-cart-plus" />
                                        <FaSearch className="fa-cart-plus" />
                                        <FaBalanceScale className="fa-cart-plus" />
                                    </div>
                                    <div className="product-txt">
                                        <h1 className="product-name">Womens Hiking Hat</h1>
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <img className="stars-images" src="../img/star.png" />
                                        <h1 className="price_product">$26.55-$29.99</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </section>
            <div className="container">
                <div className="today-fashion">
                    <div className="fashion-shop">
                        <h1 className="shop-fashion">Shop Now &#8594;</h1>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="store-header">
                    <h1>Consumer Electric</h1>
                    <div className="store">
                        <div className="store-left-utensil">
                                <div className="fashion-shop-utensil">
                                    <h1 className="shop-fashion-utensil">Shop Now &#8594;</h1>
                            </div>
                        </div>
                        <div className="store-right">
                            <div className="product">
                                <div className="product-img-sofa">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Home Sofa</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-table">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Kitchen Table</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-lamp">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Table Lamp</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-chair">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Data transformer tool</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-cooker">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Electric Rice-Cooker</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-kitchen">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Kitchen Cooker</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <img className="stars-images" src="../img/star1.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-pan">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Electric Frying Pan</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                            <div className="product">
                                <div className="product-img-crusher">
                                    <FaCartPlus className="fa-cart-plus" />
                                    <FaHeart className="fa-cart-plus" />
                                    <FaSearch className="fa-cart-plus" />
                                    <FaBalanceScale className="fa-cart-plus" />
                                </div>
                                <div className="product-txt">
                                    <h1 className="product-name">Automatic Crusher</h1>
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <img className="stars-images" src="../img/star.png" />
                                    <h1 className="price_product">$26.55-$29.99</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )


};

export default SectionShopClothes;