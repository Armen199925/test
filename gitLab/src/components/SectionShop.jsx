import React from "react";
import '../css/SectionShop.css';

function SectionShop() {
    return (
        <div>
            <section className="section-shop">
                <h1 className="shop-text">Top Categories Of The Month</h1>
                <div className="shop-access">
                    <div className="fashion">
                        <h1 className="fashion-txt">Fashion</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                    <div className="furniture">
                        <h1 className="fashion-txt">Furniture</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                    <div className="shoes">
                        <h1 className="fashion-txt">Shoes</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                    <div className="sports">
                        <h1 className="fashion-txt">Sports</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                    <div className="games">
                        <h1 className="fashion-txt">Games</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                    <div className="computers">
                        <h1 className="fashion-txt">Computers</h1>
                        <div className="shop-now">Shop Now</div>
                    </div>
                </div>
            </section>
        </div>
    )
};

export default SectionShop;