import React from 'react';

import axios from 'axios';




export default class PersonList extends React.Component {
  
    state = {
      persons: []
    }
  
    componentDidMount() {
      axios.get(`/api/list`)
        .then(res => {
          const persons = [res.products];
          this.setState({ persons });
          console.log(res.products)
        })
    }
  
    render() {
      return (
        <ul>
          { this.state.persons.map(person => <li>{person[0].products}</li>)}
        </ul>
      )
    }
    
  }
