import React from "react";
import "../css/addcart.css";

function AddToCart() {
    let addCart = document.querySelector(".add-to-cart");
    let cartIco = document.querySelector(".cart-ico");
    let cartDiv = document.createElement("div");
    let cart = document.querySelector(".cart-open");
    let cartText = document.createElement("h1");
    cartText.className = "cart-text";
    let nameProduct = document.querySelector(".name-product");
    let addImg = document.querySelector(".add-img");
    let inputNumber = document.querySelector(".num");
    let deleteBtn = document.createElement("button");
    let price = document.querySelector(".dollar");
    deleteBtn.className = "delete-btn";
    deleteBtn.innerHTML = "X";
    deleteBtn.style.position = "relative";
    deleteBtn.style.width = "10px";
    deleteBtn.style.fontSize = "12px";
    deleteBtn.style.height = "12px";
    deleteBtn.style.border = "none";
    deleteBtn.style.outline = "none";
    deleteBtn.onclick = function () {
        cartDiv.style.display = "none";
    };

    // localStorage.setItem("new", JSON.stringify(obj))
    cartDiv.className = "cart-div";
    cartDiv.style.width = "303px";
    cartDiv.style.height = "100px";
    cartDiv.style.display = "flex";
    cartDiv.style.alignItems = "flex-start";
    cartDiv.style.flexDirection = "column";
    cartDiv.style.marginTop = "20px";
    cart.append(cartDiv);
    let productDiv = document.createElement("div");
    productDiv.className = "product-div"
    productDiv.style.width = "400px";
    productDiv.style.height = "100px";
    productDiv.style.display = "flex";
    productDiv.style.justifyContent = "space-around";
    productDiv.style.alignItems = "center";
    cartDiv.append(deleteBtn);
    let img = document.createElement("img");
    img.className = "del-img";
    img.style.width = "150px";
    img.style.height = "100px";
    productDiv.append(img)
    let productName = document.createElement("h1");
    productName.style.margin = "0";
    productName.style.padding = "0";
    productName.style.fontSize = "13px";
    productName.style.width = "60px";
    productName.style.height = "60px";
    productName.style.color = "black";
    productDiv.append(productName);
    let priceDiv = document.createElement("h1");
    priceDiv.className = "price";
    priceDiv.style.fontSize = "15px";
    priceDiv.style.color = "black";
    priceDiv.style.height = "60px";
    productDiv.append(priceDiv)
    cartDiv.append(productDiv);
    let dollar = document.createElement("h1");
    dollar.innerHTML = price.innerHTML;
    dollar.style.width = "60px";
    dollar.style.color = "black";
    dollar.style.fontSize = "17px";
    productDiv.append(dollar)

    if (inputNumber.value > 1) {
        let a = inputNumber.value * price.innerHTML
    }
    let store = 0
    if (localStorage.getItem("new") == null) {
        store = 0
    } else {
        store = JSON.parse(localStorage.getItem("new"));
    }
    img.src = store.img;
    productName.innerHTML = store.name;
    priceDiv.innerHTML = store.count;
    if (store.id == 1) {
        let sc = +store.count + +inputNumber.value
        let pc = (+store.price + inputNumber.value * price.innerHTML.substring(1))
        let obj = {
            id: 1,
            img: addImg.src,
            name: nameProduct.innerHTML,
            price: pc,
            count: sc
        }
        localStorage.setItem("new", JSON.stringify(obj))
    }
    else {
        let pc = (+inputNumber.value * +price.innerHTML.substring(1))
        let obj = {
            id: 1,
            img: addImg.src,
            name: nameProduct.innerHTML,
            price: pc,
            count: inputNumber.value
        }
        localStorage.setItem("new", JSON.stringify(obj))
    }

    store = JSON.parse(localStorage.getItem("new"));
    img.src = store.img;
    let sc = +store.count + +inputNumber.value
    let pc = (+store.price + inputNumber.value * price.innerHTML.substring(1))
    productName.innerHTML = store.name;
    priceDiv.innerHTML = store.count;
    let obj = {
        id: 1,
        img: addImg.src,
        name: nameProduct.innerHTML,
        price: "sdasda",
        count: "asdasda"
    }
};

export default AddToCart;
