import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import axios from 'axios';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Blog from './Blog/Blog';
import Header from './components/Header';
import Section from './components/Section';
import SectionBottom from './components/SectionBottom';
import BuySection from './components/BuySection';
import SectionShop from './components/SectionShop';
import SectionItems from './components/SectionItems';
import SectionShopClothes from './components/SectionShopClothes';
import OurClients from './components/OurClients';
import OurBlog from './components/OurBlog';
import Recent from './components/Recent';
import SectionEmail from './components/SectionEmail';
import Footer from './components/Footer';
import PersonList from './components/PersonList';
import CreateCart from './components/CreateCart';
import Contact from './Contact/Contact';

const root = ReactDOM.createRoot(document.getElementById('root'));

const Routing = () => {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path='/' exact = {true} element={<App />} />
        <Route path="/create-cart" element={<CreateCart />} />
        <Route path="/section" element={<Section />} />
        <Route path="/section-bottom" element={<SectionBottom />} />
        <Route path="/buy-section" element={<BuySection />} />
        <Route path="/section-shop" element={<SectionShop />} />
        <Route path="/section-items" element={<SectionItems />} />
        <Route path="/section-shop-clothes" element={<SectionShopClothes />} />
        <Route path="/our-clients" element={<OurClients />} />
        <Route path="/our-blog" element={<OurBlog />} />
        <Route path="/recent" element={<Recent />} />
        <Route path="/section-email" element={<SectionEmail />} />
        <Route path="/Blog" element={<Blog />} />
        <Route path="/Contact" element={<Contact />}/>
      </Routes>
      <SectionEmail />
      <Footer />
      <PersonList />
    </Router>
  )
}

root.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
);




// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
