
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function (app) {
    app.use(
        '/api',
        createProxyMiddleware({
            target: 'https://37.186.119.16:9443',
            changeOrigin: true,
            secure: false,
        })
    );
};